﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SendGrid.Helpers.Mail;
using SendGrid;

using System.Text;

namespace TrySendGrid.Controllers
{
    public class HomeController : Controller
    {
        public void sendMail()
        {
            #region 設定MAIL內容
            StringBuilder sb = new StringBuilder("OOO的Azure SendGrid功能測試：");
            sb.AppendLine("<table border='1'>");
            sb.AppendLine("<tr><th>Hi Everyone this is my test message</th></tr>");
            sb.AppendLine("<tr><td>1. I find a nice api in Azure ,It's made easy to send mail.</td></tr>");
            sb.AppendLine("<tr><td>2. Just some nonsense by myself.</td></tr>");
            sb.AppendLine("<tr><td>3. That's all thx.</td></tr>");
            sb.AppendLine("</table>");
            var mailContent = sb.ToString();
            #endregion
            string AzureWebJobsSendGridApiKey = "填入自己的ApiKey";
            dynamic sg = new SendGridAPIClient(AzureWebJobsSendGridApiKey);
            SendGrid.Helpers.Mail.Email from = new SendGrid.Helpers.Mail.Email("OOO@outlook.com", "OOO");
            string subject = "OOO的測試郵件";
            Email to = new Email("aaa@gmail.com");
            Content content = new Content("text/html", mailContent);
            Mail mail = new Mail(from, subject, to, content);

            Personalization personalization = new Personalization();
            string[] mailcc = "bbb@gmail.com,ccc@gmail.com".Split(',');
            if (mailcc != null)
            {
                foreach (string cc in mailcc)
                {
                    //這邊卡很久，一開始是使用AddCc，但不Work，改為AddTo才行!                                 
                    personalization.AddTo(new Email(cc));
                }
            }
            mail.AddPersonalization(personalization);

            dynamic response = sg.client.mail.send.post(requestBody: mail.Get());
        }
    }
}