﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TrySendGrid.Startup))]
namespace TrySendGrid
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
